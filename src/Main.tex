\documentclass{acm_proc_article-sp}
\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{balance}
\usepackage{booktabs}
\usepackage{array}
\usepackage{url}
\usepackage{hyperref}
\usepackage{caption}
\DeclareCaptionType{copyrightbox}
\usepackage{subcaption}
\usepackage{todonotes}
\usepackage{listings}
\presetkeys{todonotes}{inline}{}
\usepackage[numbers]{natbib}
\usepackage{paralist}
\usepackage{lmodern}
\newenvironment{ipenum}{\begin{inparaenum}[(i)]}{\end{inparaenum}}
\graphicspath{../imgs/}
\usepackage{etoolbox}

\newtoggle{comments}
\toggletrue{comments}
%\togglefalse{comments}
\newcommand{\notebl}[1]{\iftoggle{comments}{\todo[color=yellow]{{[}BL{]} #1}{}}}
\newcommand{\notepw}[1]{\iftoggle{comments}{\todo[color=green]{{[}PW{]} #1}{}}}
\begin{document}\title{Benchmarking of Java based stream reasoning engines}

\author{Bernhard Ortner, Marcus Presich, Peter Wetz}

\date{\today}

\begin{document}
\maketitle

\begin{abstract}
Your abstract.x
\end{abstract}

\section{Introduction}
Since Tim Berners-Lee proposed the semantic web~\footnote{\url{http://www.ted.com/talks/tim_berners_lee_on_the_next_web?language=en}} in 2006, since then the world wide web as we know it has transformed tremendously. 
Large amounts of data are produced by us, the users of the internet everyday, by writting blog posts or posting social media entries. 
Scientists have been estimated that every minute on the world wide web 600 videos are uploaded on Youtube, 168 millions e-mails are sent via an email service, 510,000 entries are commented on Facebook and 98,000 tweets are send via Twitter.~\footnote{3}
When having a closer look at such numbers, we can see that it is often the case that information, which is instantly available can be rather outdated quickly.  
This problem leads to the development of applications, which can make information available in real time, such as Data Stream Management Systems (DSMSs)~\footnote{3} and Complex Event Processing (CEP) Systems.~\footnote{3}
The strength of such systems is, that they usually have small delays of pushing the data forward to another application, which processes the data.
Furthermore, such systems rely on specific data model to allow specific operations on manipulating the input stream.
Such operations are for example, ad-hoc optimization to improve the processing performance.
As Tim Berners-Lee~\footnote{•} proposed in 2006, data should be linked via the RDF format, so that unstructured data can be used in a structured way. Therefore, many application architects have made the choice in building their application using the RDF format. 

The initial idea is to link these data sources together to create a large knowledge base, however, scientists have thought a step ahead and came up with the idea of "reasoning" these data sources. Hence, they have implemented stream reasoning engines, which  use complex algorithms to infer further knowledge about their existing knowledge base.~\footnote{•}
5 years ago this research field was defined as an “unexplored yet high impact research area”~\footnote{•}.
Today after some years of research, stream reasoning is still in its infancy and a lot of open questions are still unexplored.
However, over the years a lot of new stream reasoning engines have been created. 
\textbf{Contribution.} In this paper we want to show a detailed benchmarking test of the currently available Java based stream reasoning engines. We are testing CSPARQL, Sparkwave and CQLES.

\textbf{Outline.} This paper is organised into several different sections.
First we want to show the problem of the current benchmarking tests and the releated work. Furthermore, we will focus on our methodological approach. After this sections, we will propose an architectural design of our application. Finally, we will show the results of our benchmarking tests and draw a conclusion over the current Java based stream reasoning engines.


\section{Related Work}
Current proposed streaming benchmark tests have been developed from relational benchmarking tests, e.g. Linear Road (LR) and have been adapted to RDF Streaming engines. LR benchmark, a Data Stream Management Systems (DSMS) benchmark test measures the throughput and validity of such systems by parametrizing the input data modeling bursts or streaming data with a high throughput~\cite{arasu2004linear}.\newline
%LUBM, BSBM, SP2Bench = traditionelle benchmark tests
SRBench  is a continuous development of the LR benchmark towards the goal to measure streaming Resource Description Framework (RDF) triples by mocking RDF data from the Linked Open Data (LOD) cloud. SRBench's main purpose is to assess the functionality of the tested engine~\cite{zhang2012srbench}. \newline
Furthermore both tests assume that engines return a valid answer. Dell�Aglio \emph{et al} argued that engines may switch to an invalid state if they evaluate complex join queries over multiple data streams~\ref{dell2013need}.\newline
This issue is addressed by the CSRBench, which adds correctness test to the SRBench. The correctness proof is performed by CSRBench's oracle. 
%\footnote{\url{https://github.com/dellaglio/csrbench-oracle/blob/master/src/main/java/eu/planetdata/srbench/oracle/Oracle.java}}
It parametrizes and checks the returned query based on a description of the streaming system $M$. This description defines the theoretical answer of the streaming system $r^{O}$. When a query is executed by the description on the test engine the engine produces a result set $r^{S}$. A test is said to be correct if and only if $r^{O} = r^{S}$~\ref{dell2013correctness}. 

\section{Problem Description}
CSRBench has some limitations regarding the variety of the queries and the architecturs. When this paper was written\footnote{24.3.2015} CSRBench's oracle was able to generate queries of a single window. Dell'Aglio \emph{et al} also mentioned that CSRBench is designed to cope with DSMS.\newline
However one frequently choosen use case is defining multiple windows within a single engine. In this use case an engine has to ensure the correctness of every registered window. A window is correct if the arrival timestamp of the tuples is continuously increasing.%citation needed 
Furthermore a benchmark test has to encapsulate the implementation details of engine properly for ensuring that the results are compareable and independent of the underlying engine.\newline
Additional to the above listed drawbacks, the correctness proof is only partial achieved. Lamport has defined that a correctness proof consists of two tests.
First \textit{safety} has to be checked. He defined safety the state of an engine where something $\lgqq$unwanted$\grqq$ is not occuring, e.g. an error or an occurence of a faulty data.
Second \textit{liveness} has to be addressed. It is defined as something that is expected to happen, e.g. a correct result of a data stream or an event\cite{lamport1977proving}. Consequently
CSRBench's correctness is only partial fulfilled, i.e. the safety proof is missing.\newline

\section{Architectural Design}
%
% Insert Picture of overall architecture here
%

The major goal of the benchmark test is to generate syntetic reproduceable data from multiple streams and measure the capabilities of the engine afterwards. The test allows the selection of different engines, e.g. CQELS, C-Sparql or Sparkwave. Each engine is implemented as own module and runs on a separate threadpool. A benefit of such modularization is that a new engine is testable with less effort and its measurements are more precise and suffer from less side effects from parallel threads. \newline
\subsection{Data generation}
As use case for data generation is the room example of the RDF Streaming processing group selected. Figure~\ref{fig:RoomOntology} displays the used ontology for the use case.  

\begin{figure}[htp]
	\centering
		\includegraphics[width=0.3\textwidth]{figures/RoomOntology.png}
	\caption{Room Ontology}
	\label{fig:RoomOntology}
\end{figure}

The ontology is used for each separate room and generates a data stream containing a defined amount of observations. These observations have a post describing entering and leaving a room randomly and generating room enter and room leave events. The events are measured by a sensor and both of them are queried by the engine, i.e. give me the persons that have met in a room.
In the next step the capabilities of the engines, e.g. throughput, queue size are measured by counting the returned parameter from the query window.
%If a new engine is implemented it has to implement the IEngine interface. The same procedure is applied if a new property has to be trackt. In that case the end-user has to implement a appropriate listener, i.e. IResultListener or IMeasurementListener. If one of these listener is implemented, the underlying engine sends the specific event when it is available or raised.

\subsection{Event-Listening}

The properties for the measurment are obtained from the engine by registering Listener. A Listener is an abstract interface for a piece of code that perform some operations on the output of the engines i.e. performing some measurements or correctness proofs. Currently two kind of listeners are implemented: The IMeasurementListener is designed for measuring a specific property of each Engine, and IResultListeners are designed for tracking or processing the output of engines. Once they are registered on the engine they get all incoming events and continue processing depending on their implementation. Currently following four listeners are implemented: 

\begin{enumerate}
	\item ConsoleListener: \newline The ConsoleListener prints incoming events without any further processing steps on the commandline. 
	\item CSVListener: \newline Similar to the ConsoleListener, this listener saves incoming events to a Comma Separated Value (CSV) file. This files are used for analysing bottle necks and regression analysis lateron.
	\item CorrectnessListener\newline This listener validates if a window is correct i.e. it measures if an expected input value implies a corresponding output event. Furthermore it checks if there occured any events that are not expected, e.g. errors from the engine.
    \item AggregationListener\newline The Aggregationlistener accumulates all incoming events within an user defined time span, e.g. a second. The purpose of this listener is to aggregate data for displaying them in the Graphical User Interface (GUI) of the benchmark test. 
\end{enumerate}

\section{Test}

The tests of the benchmark run isolated from the generator in an own threadpool to get a more precise measurement of the engine. A measurement consists of a generator that feeds data into an engine and specific engine dependent queries\footnote{each Engine has its own query language}. An user can select between predefined queries or he has the option to submit a customized query for measuring its performance. Following properties are collected by IMeasurementListener. Each property requires a registration of an instance of a listener, i.e. a listener is able to collect only one property source.

\begin{enumerate}
	\item Correctness from the CorrectnessListener
	\item Throughput from the AggregationListener
	\item Queuesize from the AggregationListener
\end{enumerate}

The user is able to select three predefined queries and address a different test purpose. The complexity of the queries are continuously increasing.

\subsection{Base Query}
\label{q:base}
The goal of the first test case is to test the basis functionality of the engine, i.e. detecting Persons in a room.

\subsection{Temporal Aggregation Query}

This query aims to test aggregation of tuples, i.e. calcluate the average duration of a person within a room. 

\subsection{Scaling Query}
The last query tries to evaluate the capabilities of an engine by scaling query~\ref{q:base}.

\section{Evaluation}

The benchmark test is evaluated on a decentralized virtual machine. Additional when a test is ran, a new engine is instrumentalized by running a cold start phase to exclude the initial caching and other side effects of the start, i.e. a query is sampled until it returns the expected result. After that the measurements of the engine starts and all listeners are registered to an engine. When the test ends, i.e. the entered evaluation period of the user is over, the overall correctness of the engine is proven.

\section{Results \& Conclusion}
our outcome of our SRBP tests with !!graphs!!


\section{Future Work}
RSP Syntax einpflegen und mit einer referenzimplementierung vergleichen...


\newpage

\bibliographystyle{plain}
\bibliography{bibliography}

\end{document}